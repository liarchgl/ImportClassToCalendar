# ImportClassToCalendar

功能：将课程导入outlook、IOS、google等邮箱日历或日历应用当中

# My Environment

- System:Win10
- Python:3.6
- Install moudle:
```
pip install xlrd
```

# Usage

1、配置 `classInfo` Excel 表格

在工作目录下创建一个 `classInfo` Excel 表格，表格表头有如下参数：  

```
className：课程名称  
startWeek：起始周  
endWeek：结束周  
weekday：星期  
classTime：第几节。这个参数要配合 `conf_classTime.json` 文件，下文会讲。  
classroom：教室
```

2、配置 `conf_classTime.json`

来吧，照猫画虎：

```json
{
  "classTime": [
    {
        "name":"第 1、2 节",
        "startTime":"0830",
        "endTime":"1005"
    },
    {
        "name":"第 3、4 节",
        "startTime":"1025",
        "endTime":"1200"
    },
    {
        "name":"第 5、6、7 节",
        "startTime":"1350",
        "endTime":"1615"
    },
    {
        "name":"第 8、9 节",
        "startTime":"1630",
        "endTime":"1800"
    },
    {
        "name":"第 10、11、12 节",
        "startTime":"1830",
        "endTime":"2100"
    }
  ]
}
```

3、运行 `excelReader.py` 脚本

终端运行：  
```
python excelReader.py
```

![null](http://chanjh.b0.upaiyun.com/0031/excelReader.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

如果 `classInfo` 表格是按照 Demo 的样式保存的，输入 0 继续即可。如果不是……你改回来就可以。（一个小脚本而已，懒得做那么多容错啦～）

完成后生成一个 `conf_classInfo.json` 配置文件

4、运行 main.py 脚本

终端运行：  
```
python main.py
```  

![null](http://chanjh.b0.upaiyun.com/0031/main.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

回车后，需要输入两个参数：第一周的星期一日期、提醒时间。

提醒时间就是在 iOS、Mac 上创建日历时所使用的设置，不知道这样的设置在 Android 和 Windows 是否生效。待测。

完成后在工作目录下会生成一个 class.ics 文件

5、导入手机或者电脑

如果你是 Mac + iOS， 恭喜你，直接点开 class.ics 就可以导入 Mac， 然后等待 iCloud 同步就可以。

如果想单独导入 iOS，可以用邮件将 ics 发送到你的 iOS 可以收到的邮箱里，在邮箱内打开就可以。

Android 和 Windows 的导入方法我没试过，不过我觉得应该和 iOS、Mac 差不多的。

![null](http://chanjh.b0.upaiyun.com/0031/日历.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

# Info:
[转载自陈某豪——少数派](https://sspai.com/post/39645)